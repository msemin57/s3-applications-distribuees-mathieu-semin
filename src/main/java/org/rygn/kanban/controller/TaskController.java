package org.rygn.kanban.controller;

import org.rygn.kanban.domain.Task;
import org.rygn.kanban.service.TaskService;
import org.rygn.kanban.utils.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.validation.annotation.Validated;

import java.util.Collection;

@RestController
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping(value = "/tasks", produces = "application/json")
    public Collection<Task> getTasks()
    {
        Collection<Task> tasks = this.taskService.findAllTasks();
        return tasks;
    }

    @PostMapping(value = "/tasks")
    public Task addTask(@Validated @RequestBody Task task)
    {
        Task _task = new Task();
        this.taskService.createTask(_task);
        return _task;
    }

    @PatchMapping(value = "/tasks/{id}")
    public Task  moveTaskStatus(@PathVariable long id, @RequestBody String leftRight)
    {
        Task taskToMove = this.taskService.findTask(id);
        if (leftRight == Constants.MOVE_LEFT_ACTION)
        {
            this.taskService.moveLeftTask(taskToMove);
        }
        else if (leftRight == Constants.MOVE_RIGHT_ACTION)
        {
            this.taskService.moveRightTask(taskToMove);
        }
        return taskToMove;
    }
}
