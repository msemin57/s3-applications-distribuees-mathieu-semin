package org.rygn.kanban;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@WebAppConfiguration
@ActiveProfiles(profiles = "test")
class DeveloperControllerTest {


    @Autowired
    public MockMvc _MockMVC;
    @Autowired
    WebApplicationContext webApplicationContext;

    @Test
    void findAllDevelopers() throws Exception {
        _MockMVC = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        MvcResult mvcResult = _MockMVC.perform(MockMvcRequestBuilders.get("/developers").accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String response = mvcResult.getResponse().getContentAsString();
        assertEquals(JsonPath.parse(response).read("$[0].lastname"),"dev1");
    }
}