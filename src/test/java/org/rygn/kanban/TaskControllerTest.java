package org.rygn.kanban;

import org.rygn.kanban.domain.Task;
import org.rygn.kanban.domain.TaskStatus;
import org.rygn.kanban.domain.TaskType;
import org.rygn.kanban.service.TaskService;
import org.rygn.kanban.utils.Constants;

import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@WebAppConfiguration
@ActiveProfiles(profiles = "test")

class TaskControllerTest {

    @Autowired
    public MockMvc mvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    private TaskService taskService;

    @Autowired
    private ObjectMapper objMapper;


    @Test
    void findAllTasks() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/tasks").accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String response = mvcResult.getResponse().getContentAsString();
        assertEquals(JsonPath.parse(response).read("$[0].title"),"task1");
    }

    @Test
    void addTasks() throws Exception{


        Task _task = new Task();
        _task.setTitle("_testTask");
        _task.setNbHoursForecast(6);
        _task.setNbHoursReal(10);

        TaskType _typeTest = new TaskType(Constants.TASK_TYPE_BUG_ID, Constants.TASK_TYPE_BUG_LABEL);
        _task.setType(_typeTest);

        TaskStatus _statusTest = new TaskStatus(Constants.TASK_STATUS_DOING_ID, Constants.TASK_STATUS_DOING_LABEL);
        _task.setStatus(_statusTest);

        String taskJson = objMapper.writeValueAsString(_task);

        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/tasks").contentType(MediaType.APPLICATION_JSON_VALUE).content(taskJson)).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());
    }

    @Test
    void moveTaskTest() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        Task _task = new Task();
        _task.setTitle("task_test");
        _task.setNbHoursForecast(6);
        _task.setNbHoursReal(10);

        TaskType _typeTest = new TaskType(Constants.TASK_TYPE_BUG_ID, Constants.TASK_TYPE_BUG_LABEL);
        _task.setType(_typeTest);

        TaskStatus _statusTest = new TaskStatus(Constants.TASK_STATUS_DOING_ID, Constants.TASK_STATUS_DOING_LABEL);
        _task.setStatus(_statusTest);

        String taskJson = objMapper.writeValueAsString(_task);

        ArrayList<Task> tasks = (ArrayList<Task>) taskService.findAllTasks();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.patch("/tasks/"+tasks.get(1).getId()).contentType(MediaType.APPLICATION_JSON_VALUE).content(objMapper.writeValueAsString(Constants.MOVE_RIGHT_ACTION))).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());
    }
}